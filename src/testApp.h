#pragma once

#include "ofMain.h"
#include "ofxXmlGui.h"
#include "Scanner.h"
#include "ofxMidi.h"
class testApp : public ofBaseApp, public ofxXmlGuiListener, public ofxMidiListener {

public:
	void setup();
	void update();
	void draw();
	void controlChanged(ofxXmlGuiEvent *e);
	void keyPressed  (int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);
	void noteOn(int note, int vel);
	void noteOff(int note);
	vector<Scanner> scanners;
	
	void newMidiMessage(ofxMidiEventArgs& eventArgs);
	void setupMidi();
	
	ofxMidiIn midiIn;
	ofxMidiOut midiOut;
};
