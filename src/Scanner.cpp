/**
 *  Scanner.cpp
 *
 *  Created by Marek Bereza on 02/06/2013.
 */

#include "Scanner.h"

ImageSequence Scanner::scannerOn;
ImageSequence Scanner::scannerOff;
void Scanner::init() {
	scannerOn.load("scannerOn");
	scannerOff.load("scannerOff");
}
