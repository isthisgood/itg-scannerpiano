/**     ___           ___           ___                         ___           ___     
 *     /__/\         /  /\         /  /\         _____         /  /\         /__/|    
 *    |  |::\       /  /::\       /  /::|       /  /::\       /  /::\       |  |:|    
 *    |  |:|:\     /  /:/\:\     /  /:/:|      /  /:/\:\     /  /:/\:\      |  |:|    
 *  __|__|:|\:\   /  /:/~/::\   /  /:/|:|__   /  /:/~/::\   /  /:/  \:\   __|__|:|    
 * /__/::::| \:\ /__/:/ /:/\:\ /__/:/ |:| /\ /__/:/ /:/\:| /__/:/ \__\:\ /__/::::\____
 * \  \:\~~\__\/ \  \:\/:/__\/ \__\/  |:|/:/ \  \:\/:/~/:/ \  \:\ /  /:/    ~\~~\::::/
 *  \  \:\        \  \::/          |  |:/:/   \  \::/ /:/   \  \:\  /:/      |~~|:|~~ 
 *   \  \:\        \  \:\          |  |::/     \  \:\/:/     \  \:\/:/       |  |:|   
 *    \  \:\        \  \:\         |  |:/       \  \::/       \  \::/        |  |:|   
 *     \__\/         \__\/         |__|/         \__\/         \__\/         |__|/   
 *
 *  Description: 
 *				 
 *  Scanner.h, created by Marek Bereza on 02/06/2013.
 */
#pragma once

#include "ofMain.h"
#include "ImageSequence.h"
#include "ofxMidi.h"

class Scanner {
public:
	static ImageSequence scannerOn;
	static ImageSequence scannerOff;
	static void init();
	
	int midiChannel;
	float animPos;
	int note;
	int animDirection;
	ofxMidiOut *midiOut;
	Scanner() {
		animDirection =1;
		animPos = 0;
		midiChannel = 0;
		note = 0;
	}
	void setup(int midiChannel, ofxMidiOut *midiOut) {
		this->midiChannel = midiChannel;
		this->midiOut = midiOut;
		sound.loadSound("scanner.wav");
		sound.setLoop(true);
	}
	
	void noteOn(int note, int vel) {

		float n = note - 21;
		// how far away from low a
		float speed = pow(2, n/12.f);
		sound.setSpeed(speed);
		sound.play();
		if(this->note!=0) {
			printf("Note is already beeng plaid\n");
		} else {
			this->note = note;
			midiOut->sendNoteOn(midiChannel, note, 100);
		}
	}
	
	void noteOff(int note) {
		sound.stop();
		if(this->note==note) {
			this->note = 0;
			midiOut->sendNoteOff(midiChannel, note, 100);
		} else {
			printf("Wrong note!!\n");
		}
	}
	
	
	int getNote() {
		return note;
	}
	
	ofSoundPlayer sound;

	void draw(float x, float y, float scale) {
		if(this->note!=0) {
			animPos += animDirection*ofMap(note, 20, 60, 0.02, 0.1, true);
			if(animDirection==1 && animPos>=scannerOn.size()) {
				animDirection = -1;
				animPos = scannerOn.size() - 1;
			}
			if(animDirection==-1 && animPos<=0) {
				animDirection = 1;
				animPos = 0;
			}
		}
		if(this->note!=0) {
			if(animPos<scannerOn.size()) {
				scannerOn[(int)animPos].draw(x, y, scannerOn[(int)animPos].getWidth()*scale, scannerOn[(int)animPos].getHeight()*scale);
			} else {
				printf("Anim image out of range %d\n", animPos);
			}
		} else {
			if(animPos<scannerOff.size()) {
				scannerOff[(int)animPos].draw(x, y, scannerOn[(int)animPos].getWidth()*scale, scannerOn[(int)animPos].getHeight()*scale);
			} else {
				printf("Anim image out of range %d\n", animPos);
			}
		}
	}
};