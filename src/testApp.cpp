#include "testApp.h"
int noteOffset = -12;
//--------------------------------------------------------------
void testApp::setup(){
	Scanner::init();
	setupMidi();
	int numScanners = 8;
	for(int i = 0; i < numScanners; i++) {
		int channel = i+1;
		if(channel==6) continue;
		scanners.push_back(Scanner());
		scanners.back().setup(channel, &midiOut);
	}
	
	ofEnableAlphaBlending();
}

void testApp::setupMidi() {
	midiIn.listPorts();
	int portId = 2;
	for(int i = 0; i < midiIn.portNames.size(); i++) {
		if(midiIn.portNames[i].find("USB MIDI Interface")!=-1) {
			portId = i;
			break;
		}
	}
	
	midiIn.openPort(portId);
	midiIn.addListener(this);
	midiOut.openPort(portId);
	
}

void testApp::controlChanged(ofxXmlGuiEvent *e) {

}
void testApp::noteOn(int note, int vel) {
	note += noteOffset;
	bool foundScanner = false;
	vector<Scanner*> scannies;
	for(int i = 0; i < scanners.size(); i++) {
		if(scanners[i].getNote()==0) {
			scannies.push_back(&scanners[i]);
		}
	}
	
	if(scannies.size()==0) {
		printf("Must steal voice!!\n");
		// ALSO TODO - turn on multiple scanners with one note?
	} else {
		int index = ofRandom(0, scannies.size())*0.999;
		scannies[index]->noteOn(note, vel);
	}
}

void testApp::noteOff(int note) {
	note += noteOffset;
	bool foundScanner = false;
	for(int i = 0; i < scanners.size(); i++) {
		if(scanners[i].getNote()==note) {
			scanners[i].noteOff(note);
			foundScanner = true;
			break;
		}
	}
	if(!foundScanner) {
		printf("Couldn't find scanner to turn off!!\n");
	}
}

void testApp::newMidiMessage(ofxMidiEventArgs& m) {
	if(m.status==MIDI_NOTE_ON) {
		printf("Note on, %d %d\n", 		m.byteOne, m.byteTwo);
		if(m.byteTwo>0) {
			noteOn(		m.byteOne, m.byteTwo);
		} else {
			noteOff(m.byteOne);
		}
	} else if(m.status==MIDI_NOTE_OFF) {
		printf("Note off %d %d\n", m.byteOne, m.byteTwo);
		noteOff(m.byteOne);
	}
}
//--------------------------------------------------------------
void testApp::update(){

}

//--------------------------------------------------------------
void testApp::draw(){
	ofBackground(22, 33, 44);
	float w = 241;
	for(int i = 0; i < scanners.size(); i++) {
		scanners[i].draw(20+(i%5)*200, 20+(i/5)*250, 0.6);
	}
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
	if(key=='p') {
		for(int ch = 0; ch <=16; ch++) {
			for(int n = 0; n < 128; n++) {
				midiOut.sendNoteOff(ch, n, 0);
			}
		}
	}
	if(key=='a') {
		noteOffset += 12;
	} else if(key=='z') {
		noteOffset -= 12;
	}
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}