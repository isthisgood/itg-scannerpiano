/**     ___           ___           ___                         ___           ___     
 *     /__/\         /  /\         /  /\         _____         /  /\         /__/|    
 *    |  |::\       /  /::\       /  /::|       /  /::\       /  /::\       |  |:|    
 *    |  |:|:\     /  /:/\:\     /  /:/:|      /  /:/\:\     /  /:/\:\      |  |:|    
 *  __|__|:|\:\   /  /:/~/::\   /  /:/|:|__   /  /:/~/::\   /  /:/  \:\   __|__|:|    
 * /__/::::| \:\ /__/:/ /:/\:\ /__/:/ |:| /\ /__/:/ /:/\:| /__/:/ \__\:\ /__/::::\____
 * \  \:\~~\__\/ \  \:\/:/__\/ \__\/  |:|/:/ \  \:\/:/~/:/ \  \:\ /  /:/    ~\~~\::::/
 *  \  \:\        \  \::/          |  |:/:/   \  \::/ /:/   \  \:\  /:/      |~~|:|~~ 
 *   \  \:\        \  \:\          |  |::/     \  \:\/:/     \  \:\/:/       |  |:|   
 *    \  \:\        \  \:\         |  |:/       \  \::/       \  \::/        |  |:|   
 *     \__\/         \__\/         |__|/         \__\/         \__\/         |__|/   
 *
 *  Description: 
 *				 
 *  ImageSequence.h, created by Marek Bereza on 02/06/2013.
 */

#include "ofMain.h"


class ImageSequence {
public:
	
	bool loaded;
	
	ImageSequence() {
		loaded = false;
	}
	
	void load(string dirPath) {
		printf("Loading %s\n", dirPath.c_str());
		ofDirectory dir;
		dir.allowExt("png");
		int numFiles = dir.listDir(dirPath);
		for(int i = 0; i < numFiles; i++) {
			images.push_back(ofImage());
			images.back().loadImage(dir[i].path());
		}
		loaded = true;
		printf("Found %d images\n", images.size());
	}
	int size() {
		return images.size();
	}
	
	ofImage & operator [] (const int &i) {
		return images[i];
	}
private:
	vector<ofImage> images;

};